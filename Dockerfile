FROM python:3.7

ENV PYTHONUNBUFFERED 1

COPY ./web/requirements.txt .

RUN python -m pip install --upgrade pip

RUN pip install -r requirements.txt

COPY ./web .
